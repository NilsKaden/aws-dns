package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

func getCurrentIP() string {
	resp, err := http.Get("http://api.ipify.org/")
	var bodyString string
	if err != nil {
		os.Stderr.WriteString(err.Error())
		os.Stderr.WriteString("\n")
		os.Exit(1)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
		}
		bodyString = string(bodyBytes)
		fmt.Println(bodyString)
	}
	return bodyString
}

func generateUpdateJSONWithCurrentIP(ipString string) {
	b, err := ioutil.ReadFile("./updateSchema.json")
	if err != nil {
		fmt.Print(err)
	}

	jsonString := string(b)
	updatedJSONString := strings.Replace(jsonString, "$IP$", ipString, 1)
	fmt.Println(updatedJSONString)

	ioutil.WriteFile("update.json", []byte(updatedJSONString), 0644)
}

func runAWSUpdate() {
	command := "aws"
	arg0 := "route53"
	arg1 := "change-resource-record-sets"
	arg2 := "--hosted-zone-id"
	arg3 := "Z2U1YL08ZOEYD7"
	arg4 := "--change-batch"
	arg5 := "file://update.json"

	cmd := exec.Command(command, arg0, arg1, arg2, arg3, arg4, arg5)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Print(string(stdout))
}

func main() {
	// if update.json already exist, we delete it
	if _, err := os.Stat("update.json"); err == nil {
		os.Remove("update.json")
	}

	ip := getCurrentIP()
	generateUpdateJSONWithCurrentIP(ip)
	runAWSUpdate()
	os.Remove("update.json")
}
